---
title: "About"
date: 2020-12-06T16:56:20-08:00
draft: false
---
![I'm on the mountain top](/images/avatar.jpeg)
\
I am a husband, father, leader and software engineer.\
\
I am a Senior Principal engineer with [Plexus](https://www.plexus.com) and thankful to work with a talented, committed, energetic group of people. I have and continue to have the opportunity to develop several products from concept to production. The products have a positive impact and the work is challenging. This allows me to keep up with new technologies an process; continuously practice and improve leadership skills; and bring this knowledge to problems needing solutions.\
\
I have studied Computer Engineering at the [Milwaukee School of Engineering](https://www.msoe.edu/) and Computer Science at the [University of Illinois at Urbana-Champaign](https://illinois.edu/). I have studied leadership through experience doing product development and being a parent.\
\
Perhaps you may find value and insight in the content I share. Perhaps you may be guidedfor improved quality, engagement, efficiency, ...