---
title: "Hello... again"
date: "2020-12-15T11:52:07-06:00"
featuredImage: "images/elk.jpg"
draft: false
---
I have written before, admittedly it has been awhile. My previous writing remains at [experiencesinsoftware.wordpress.com](https://experiencesinsoftware.wordpress.com/).\
\
Read a bit more about where I have been the past seven years, a comment on how parenting affects leadership, and an example team structure for mentoring\...

<!--more-->
\
My work has taken me to Malaysia, Colorado, North Carolina, and Germany. I am now back home. Throughout the past seven years my engineering, leadership, mentoring skills have continued to grow through opportunities and challenges.\
\
Being a father has grown my patience, coaching, and belief our children grow through experiences which include failure. We need to allow the risk of failure to exist. As our children grow we trust them more and allow room for greater risk.\
\
I carry this belief into my work. You must trust and empower those you put into leadership roles; then limit escalation paths, removing the safety net and confirming your trust in them.

# A team structure for mentoring
A quick thought, while saying hello, hopefully making this post useful as you have invested the time reading my "hello".\
\
Mentoring or coaching is rewarding when you are able to see a person grow and be successful. Immerse yourself in the experience and help a mentee succeed by ensuring a capable team around them. The most rewarding and successful situation I have had as a mentor is when working directly on the team.\
\
I set myself up as a developer on the team. This allows me to:

* Be in the situations a new software leader is in. These include moments spent with the customer, our management, and cross-functionally with our hardware development teams. I can observer directly and coach on situational leadership.
* Act as a developer to ease a new software leader's burden of mentoring their team. Leading is difficult, needing to mentor those you lead is even more difficult.
* Act as a developer to immerse myself in the product design and be a better mentor to other developers.

One more thought. I have over 25 years of experience and a job title to match. A new software lead does not. When I'm introducing myself in these situations, I will introduce myself to a customer as a "software engineer" with no title and not share my years of experience. I want the customer to look to the new software lead and not to rely on having me in that place.